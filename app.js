var Ping = require('./lib/Ping'),
    chalk = require('chalk'),
    monitors = [],
    websites = [
        {
            url: 'https://i.btg360.com.br/btg360-2.0.1.min.js',
            timeout: 5,//tempo em segundos
            logfile: './logs/btg360'
        },
        {
            url: 'https://i.btg360.com.br/bs.js',
            timeout: 5,//tempo em segundos
            logfile: './logs/bs'
        },
        {
            url: 'https://i.btg360.com.br/lc.js',
            timeout: 5,//tempo em segundos
            logfile: './logs/lc'
        },
        {
            url: 'https://c.btg360.com.br/__bs.gif',
            timeout: 5,//tempo em segundos
            logfile: './logs/bs_gif'
        },
        {
            url: 'https://b.btg360.com.br/__bcl.gif',
            timeout: 5,//tempo em segundos
            logfile: './logs/bcl_gif'
        }//,
        // {
        //     url: 'https://c.btg360.com.br/__bsc.gif?i=219:1&b=68b9eea7-edf5-4a45-9128-da94bb0e26e4&v=68b9eea7-edf5-4a45-9128-da94bb0e26e4-3149739278297341-9732672607027353&c=68b9eea7-edf5-4a45-9128-da94bb0e26e4-3149739278297341-9732672607027353&rand=7226314341528426&ac=apc&pa=|nv:1|np:2|ur:http%3A%2F%2Fwww.cea.com.br%2Fkit-meias-branco-408633-branco%2Fp|up:https%3A%2F%2Fwww.cea.com.br%2Fcheckout%23%2Fcart|cle:&utms=&p=|i:2017676|n:Kit%20de%206%20Meias%20Branco%20Unico|v:15.99|c:Moda%20Masculina|s:Cuecas%20e%20Meias|ss:Meias|m:C%26A'
        //     timeout: 15
        // }
    ];

websites.forEach(function (website) {
  chalk.blue('new monitor', website);
    var monitor = new Ping ({
        website: website.url,
        timeout: website.timeout,
        logfile: website.logfile
    });
    monitors.push(monitor);
});
