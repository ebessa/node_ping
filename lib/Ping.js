var fs = require('fs'),
    request = require('request'),
    chalk = require('chalk');


/*
    Ping Constructor
*/
function Ping (opts) {
    // holds website to be monitored
    this.website = '';
    // ping intervals in minutes
    this.timeout = 15;
    // interval handler
    this.handle = null;
    // initialize the app
    this.init(opts)
}

/*
    Methods
*/
Ping.prototype = {
    init: function (opts) {
        var self = this;
        self.website = opts.website;
        self.logfile = opts.logfile;
        self.timeout = (opts.timeout * 1000);
        self.JSONdata = [];
        // start monitoring
        self.start();
    },

    start: function () {
        var self = this,
            time = Date.now();

        self.createFile();
        self.ping();

        // create an interval for pings
        self.handle = setInterval(function () {
            self.ping();
        }, self.timeout);

        self.saveInterval = setInterval(function(){
          self.saveJSONdata();
        }, (1000 * 60) ); //(1000 * 60) = 1 minuto
    },

    ping: function () {
        var self = this,
            startTime = Date.now();

        try {
          // send request
          request({
            headers: {
              "Accept": "*/*",
              "Referer":"https://www.cea.com.br/checkout",
              "User-Agent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36"
            },
            uri: self.website,
            method: 'GET'
          },
          function (error, res, body) {
              // Website is up
              if (!error && res.statusCode === 200) {
                  self.isOk(startTime);
              }
              // No error but website not ok
              else if (!error) {
                  self.isNotOk(startTime, res.statusCode);
              }
              // Loading error
              else {
                  self.isNotOk(startTime);
              }
          });
        }
        catch (err) {
            self.isNotOk(currentTime);
        }
    },

    isOk: function (timeThen) {
      this.log(1, 'OK', timeThen);
    },

    isNotOk: function (timeThen, statusCode) {
        var msg = statusCode;

        this.log(0, msg, timeThen);
    },

    log: function (status, msg, timeThen) {
        var self = this,
            time = new Date(),
            day = time.getDay(),
            month = time.getMonth() + 1,
            year = time.getFullYear(),
            hour = time.getHours(),
            minutes = time.getMinutes(),
            seconds = time.getSeconds(),
            output = [
              (!status ? ('DOWN: ErrorCode ' + msg) : 'UP'),
              self.website,
              day +'/'+ month +'/'+ year,
              hour +'h'+ minutes +'m'+ seconds +'s',
              'Time to Respond: ' + ( time.getTime() - timeThen )/1000
            ].join(' - ');

        if(status){
          console.log(chalk.green(output));
        } else {
          console.log(chalk.red(output));
        }


        this.appendLogToFile(output);
        this.updateJSONdata(
          time.getTime() - timeThen,
          time,
          status
        );
    },

    createFile: function(){
      var self = this,
        time = new Date(),
        day = time.getDay(),
        month = time.getMonth() + 1,
        year = time.getFullYear(),
        hour = time.getHours(),
        minutes = time.getMinutes(),
        seconds = time.getSeconds(),
        message = "\nBEGIN log time: " + (day +'/'+ month +'/'+ year + ' - ' + hour +'h'+ minutes +'m'+ seconds +'s') + " \n\n\n";

        fs.writeFile(this.logfile + '.txt', message,
        {flag: 'w+'},
        function(err) {
          if(err) {
            console.log(err);
            'Erro ao escrever no arquivo ' + self.logfile + ' ' + err
          }
        });
    },

    appendLogToFile: function(output){
      var self = this;
      fs.appendFile(this.logfile + '.txt', output + '\n', function (err) {
        if(err){
          console.log(chalk.red('Erro ao escrever no arquivo ' + self.logfile + ' ' + err));
        }
      });
    },

    saveJSONdata: function(){
      fs.writeFile(this.logfile + '.json', JSON.stringify(this.JSONdata),
      {flag: 'w+'},
      function(err) {
        if(err) {
          console.log(err);
          'Erro ao escrever no arquivo ' + self.logfile + ' ' + err
        }
      });
    },

    updateJSONdata: function(time, reqTime, status){
      this.JSONdata.push({
        reqTime: reqTime,
        elapsed: time,
        file: this.logfile,
        status: status
      });
    },

    getFormatedDate: function (time) {
        var currentDate = new Date(time);
        currentDate = currentDate.toISOString();
        currentDate = currentDate.replace(/T/, ' ');
        currentDate = currentDate.replace(/\..+/, '');
        return currentDate;
    }
}

module.exports = Ping;
